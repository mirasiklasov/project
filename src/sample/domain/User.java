package sample.domain;

public class User {
    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private String email;
    private Password password;

    public User() {
        generateId();
    }

    public User(String name, String surname) {
        generateId();
        setName(name);
        setSurname(surname);
    }

    public User(String name, String surname, String email) {
        this(name, surname);
        setEmail(email);
    }

    private void generateId() {
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + email;
    }
}
