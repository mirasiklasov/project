package sample.repositories.interfaces;

public interface IEntityRepositories<T> {
    void add(T entity);
    void update(T entity);
    void remove(T entity);
    Iterable<T> query(String sql);
}
