package sample.repositories.interfaces;

public interface PasswordCheck {
    public boolean case1(String passtr);
    public boolean case2(String passtr);
    public boolean case3(String passtr);
}
